package user.debug.automatedtest;

import zengine.constants.ZengineMouseConstant;
import zengine.core.GameObject;

/**
 * This class does nothing and it's used just as dummy for tests.
 */
public class ObjDummy extends GameObject {

    @Override
    public void create() {
    }

    @Override
    public void destroy() {
    }

    @Override
    public void update() {
    }

    @Override
    public void draw() {
    }

    @Override
    public void collide(final GameObject other) {
    }

    @Override
    public void mouseClicked(final ZengineMouseConstant button) {
    }

}
