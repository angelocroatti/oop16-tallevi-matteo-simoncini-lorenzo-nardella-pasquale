package user.game.effects;

/**
 * This interface represent an effect emitter.
 */
public interface ObjEffectEmitter {

    /**
     * This method creates a small explosion.
     */
    void createSmallExplosion();

    /**
     * This method creates a medium explosion.
     */
    void createMediumExplosion();

    /**
     * This method creates a big explosion.
     */
    void createBigExplosion();

}
